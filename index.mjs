import * as utils from 'test-utils';
import util from 'util';

import {runMePlease} from "test-utils";

const params = {password: 'sdf234fdsf32cdsc'};

const myPromise = util.promisify(runMePlease);

try {
    const result = await myPromise(params);
    console.log(result);
} catch (error) {
    console.log(error);
}

